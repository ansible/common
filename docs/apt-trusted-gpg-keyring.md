# Migrating from trusted.gpg to trusted.gpg.d/ (Ubuntu Jammy)

```
$ apt update
[...]
W: http://ppa.launchpad.net/savoury1/digikam/ubuntu/dists/jammy/InRelease: Key is stored in legacy trusted.gpg keyring (/etc/apt/trusted.gpg), see the DEPRECATION section in apt-key(8) for details.
W: https://updates.signal.org/desktop/apt/dists/xenial/InRelease: Key is stored in legacy trusted.gpg keyring (/etc/apt/trusted.gpg), see the DEPRECATION section in apt-key(8) for details.
W: http://pub.freerdp.com/repositories/deb/jammy/dists/freerdp-nightly/InRelease: Key is stored in legacy trusted.gpg keyring (/etc/apt/trusted.gpg), see the DEPRECATION section in apt-key(8) for details.
W: http://ppa.launchpad.net/savoury1/ffmpeg4/ubuntu/dists/jammy/InRelease: Key is stored in legacy trusted.gpg keyring (/etc/apt/trusted.gpg), see the DEPRECATION section in apt-key(8) for details.
W: https://packages.riot.im/debian/dists/default/InRelease: Key is stored in legacy trusted.gpg keyring (/etc/apt/trusted.gpg), see the DEPRECATION section in apt-key(8) for details.
W: http://ppa.launchpad.net/deki/firejail/ubuntu/dists/jammy/InRelease: Key is stored in legacy trusted.gpg keyring (/etc/apt/trusted.gpg), see the DEPRECATION section in apt-key(8) for details.
W: http://ppa.launchpad.net/apandada1/foliate/ubuntu/dists/jammy/InRelease: Key is stored in legacy trusted.gpg keyring (/etc/apt/trusted.gpg), see the DEPRECATION section in apt-key(8) for details.
W: http://ppa.launchpad.net/danielrichter2007/grub-customizer/ubuntu/dists/jammy/InRelease: Key is stored in legacy trusted.gpg keyring (/etc/apt/trusted.gpg), see the DEPRECATION section in apt-key(8) for details.
W: https://download.sublimetext.com/apt/stable/InRelease: Key is stored in legacy trusted.gpg keyring (/etc/apt/trusted.gpg), see the DEPRECATION section in apt-key(8) for details.
W: http://ppa.launchpad.net/libreoffice/ppa/ubuntu/dists/jammy/InRelease: Key is stored in legacy trusted.gpg keyring (/etc/apt/trusted.gpg), see the DEPRECATION section in apt-key(8) for details.
W: http://ppa.launchpad.net/mozillateam/ppa/ubuntu/dists/jammy/InRelease: Key is stored in legacy trusted.gpg keyring (/etc/apt/trusted.gpg), see the DEPRECATION section in apt-key(8) for details.
W: http://ppa.launchpad.net/peek-developers/stable/ubuntu/dists/jammy/InRelease: Key is stored in legacy trusted.gpg keyring (/etc/apt/trusted.gpg), see the DEPRECATION section in apt-key(8) for details.
W: https://pkgs.tailscale.com/stable/ubuntu/dists/jammy/InRelease: Key is stored in legacy trusted.gpg keyring (/etc/apt/trusted.gpg), see the DEPRECATION section in apt-key(8) for details.
W: http://ppa.launchpad.net/nextcloud-devs/client/ubuntu/dists/jammy/InRelease: Key is stored in legacy trusted.gpg keyring (/etc/apt/trusted.gpg), see the DEPRECATION section in apt-key(8) for details.
W: http://ppa.launchpad.net/pbek/qownnotes/ubuntu/dists/jammy/InRelease: Key is stored in legacy trusted.gpg keyring (/etc/apt/trusted.gpg), see the DEPRECATION section in apt-key(8) for details.
W: http://ppa.launchpad.net/greymd/tmux-xpanes/ubuntu/dists/jammy/InRelease: Key is stored in legacy trusted.gpg keyring (/etc/apt/trusted.gpg), see the DEPRECATION section in apt-key(8) for details.
W: https://packagecloud.io/AtomEditor/atom/any/dists/any/InRelease: Key is stored in legacy trusted.gpg keyring (/etc/apt/trusted.gpg), see the DEPRECATION section in apt-key(8) for details.
```

+ https://askubuntu.com/questions/1407632/key-is-stored-in-legacy-trusted-gpg-keyring-etc-apt-trusted-gpg

Here's the keys in the shared (now deprecated) keyring:
```
taha@x230t:~
$ apt-key list
Warning: apt-key is deprecated. Manage keyring files in trusted.gpg.d instead (see apt-key(8)).
/etc/apt/trusted.gpg
--------------------
pub   rsa4096 2017-01-20 [SC]
      1FCD 77DD 0DBE F569 9AD2  6101 60EE 47FB AD3D D469
uid           [ unknown] Launchpad PPA for Nextcloud development

pub   rsa1024 2010-10-08 [SC]
      59DA D276 B942 642B 1BBD  0EAC A8AA 1FAA 3F05 5C03
uid           [ unknown] Launchpad PPA for Daniel Richter

pub   rsa4096 2020-02-25 [SC]
      2596 A99E AAB3 3821 893C  0A79 458C A832 957F 5868
uid           [ unknown] Tailscale Inc. (Package repository signing key) <info@tailscale.com>
sub   rsa4096 2020-02-25 [E]

pub   rsa4096 2018-10-15 [SC] [expires: 2025-05-17]
      D8BA D4DE 7EE1 7AF5 2A83  4B2D 0BB7 5829 C2D4 E821
uid           [ unknown] Brave Software <support@brave.com>
sub   rsa4096 2019-10-17 [S] [expires: 2024-05-17]

pub   dsa1024 2007-03-08 [SC]
      4CCA 1EAF 950C EE4A B839  76DC A040 830F 7FAC 5991
uid           [ unknown] Google, Inc. Linux Package Signing Key <linux-packages-keymaster@google.com>
sub   elg2048 2007-03-08 [E]

pub   rsa4096 2016-04-12 [SC]
      EB4C 1BFD 4F04 2F6D DDCC  EC91 7721 F63B D38B 4796
uid           [ unknown] Google Inc. (Linux Packages Signing Authority) <linux-packages-keymaster@google.com>
sub   rsa4096 2019-07-22 [S] [expires: 2022-07-21]
sub   rsa4096 2021-10-26 [S] [expires: 2024-10-25]

pub   rsa1024 2009-01-18 [SC]
      0AB2 1567 9C57 1D1C 8325  275B 9BDB 3D89 CE49 EC21
uid           [ unknown] Launchpad PPA for Mozilla Team

pub   rsa4096 2021-06-23 [SC] [expires: 2023-06-23]
      9FBD E02F 55F2 54D7 0082  1CCC DD3C 368A 8DE1 B7A0
uid           [ unknown] Opera Software Archive Automatic Signing Key 2021 <packager@opera.com>
sub   rsa4096 2021-06-23 [E] [expires: 2023-06-23]

pub   rsa4096 2021-01-06 [SC] [expires: 2023-02-25]
      CB63 144F 1BA3 1BC3 9E27  79A8 FEB6 023D C27A A466
uid           [ unknown] Vivaldi Package Composer KEY07 <packager@vivaldi.com>
sub   rsa4096 2021-01-06 [E] [expires: 2023-02-25]

pub   rsa4096 2019-08-19 [SC]
      E996 7359 27E4 27A7 33BB  653E 374C 7797 FB00 6459
uid           [ unknown] Launchpad PPA for Rob Savoury

pub   rsa4096 2017-12-15 [SCEA]
      0A0F AB86 0D48 5603 32EF  B581 B754 42BB DE9E 3B09
uid           [ unknown] https://packagecloud.io/AtomEditor/atom (https://packagecloud.io/docs#gpg_signing) <support@packagecloud.io>
sub   rsa4096 2017-12-15 [SEA]

pub   rsa4096 2017-05-08 [SCEA]
      1EDD E2CD FC02 5D17 F6DA  9EC0 ADAE 6AD2 8A8F 901A
uid           [ unknown] Sublime HQ Pty Ltd <support@sublimetext.com>
sub   rsa4096 2017-05-08 [S]

pub   rsa4096 2019-04-15 [SC] [expires: 2024-04-13]
      12D4 CD60 0C22 40A9 F4A8  2071 D7B0 B669 41D0 1538
uid           [ unknown] riot.im packages <packages@riot.im>
sub   rsa3072 2019-04-15 [S] [expires: 2023-04-15]

pub   rsa4096 2016-02-14 [SC]
      0B58 929C 0081 A524 10E8  9922 6053 7CDC F684 460C
uid           [ unknown] Launchpad PPA for Reiner Herrmann

pub   rsa1024 2013-03-25 [SC]
      95AC DEBD 8BFF 99AB E0F2  6A49 A507 B2BB A780 3E3B
uid           [ unknown] Launchpad PPA for Archisman Panigrahi

pub   rsa4096 2015-01-18 [SC]
      DF83 383A A43B 3648 E8BC  C441 ADD6 BF6D 97CE 5D8D
uid           [ unknown] FreeRDP builder (Used for sign autobuild packages) <team@freerdp.com>
sub   rsa4096 2015-01-18 [E]

pub   rsa1024 2010-12-29 [SC]
      36E8 1C92 67FD 1383 FCC4  4909 83FB A175 1378 B444
uid           [ unknown] Launchpad PPA for LibreOffice Packaging

pub   rsa4096 2017-02-14 [SC]
      8C95 3129 9E7D F2DC F681  B499 9578 5391 76BA FBC6
uid           [ unknown] Launchpad PPA for Peek Developers

pub   rsa4096 2015-12-30 [SC]
      FDF1 BE5B 4B02 86C8 D8B0  587F 5422 3C65 4787 8405
uid           [ unknown] Launchpad PPA for Patrizio Bekerle

pub   rsa4096 2017-04-05 [SC]
      DBA3 6B51 81D0 C816 F630  E889 D980 A174 57F6 FB06
uid           [ unknown] Open Whisper Systems <support@whispersystems.org>
sub   rsa4096 2017-04-05 [E]

pub   rsa4096 2017-02-22 [SC]
      9755 6679 87FD 9E2D 254E  D3E1 91D7 1EBF 28C3 BA9D
uid           [ unknown] Launchpad PPA for greymd

pub   rsa2048 2015-06-07 [SC]
      3960 60CA DD8A 7522 0BFC  B369 B903 BF18 61A7 C71D
uid           [ unknown] Zoom Video Communcations, Inc. Linux Package Signing Key <linux-package-signing-key@zoom.us>
sub   rsa2048 2015-06-07 [E]

/etc/apt/trusted.gpg.d/brave-browser-release.gpg
------------------------------------------------
pub   rsa4096 2018-10-15 [SC] [expires: 2025-05-17]
      D8BA D4DE 7EE1 7AF5 2A83  4B2D 0BB7 5829 C2D4 E821
uid           [ unknown] Brave Software <support@brave.com>
sub   rsa4096 2019-10-17 [S] [expires: 2024-05-17]

/etc/apt/trusted.gpg.d/google-chrome.gpg
----------------------------------------
pub   rsa4096 2016-04-12 [SC]
      EB4C 1BFD 4F04 2F6D DDCC  EC91 7721 F63B D38B 4796
uid           [ unknown] Google Inc. (Linux Packages Signing Authority) <linux-packages-keymaster@google.com>
sub   rsa4096 2019-07-22 [S] [expires: 2022-07-21]
sub   rsa4096 2021-10-26 [S] [expires: 2024-10-25]

/etc/apt/trusted.gpg.d/opera.archive.key.2019.gpg
-------------------------------------------------
pub   rsa4096 2019-09-12 [SC] [expired: 2021-09-11]
      68E9 B2B0 3661 EE3C 44F7  0750 4B8E C3BA ABDC 4346
uid           [ expired] Opera Software Archive Automatic Signing Key 2019 <packager@opera.com>

/etc/apt/trusted.gpg.d/opera.archive.key.2021.gpg
-------------------------------------------------
pub   rsa4096 2021-06-23 [SC] [expires: 2023-06-23]
      9FBD E02F 55F2 54D7 0082  1CCC DD3C 368A 8DE1 B7A0
uid           [ unknown] Opera Software Archive Automatic Signing Key 2021 <packager@opera.com>
sub   rsa4096 2021-06-23 [E] [expires: 2023-06-23]

/etc/apt/trusted.gpg.d/ubuntu-keyring-2012-cdimage.gpg
------------------------------------------------------
pub   rsa4096 2012-05-11 [SC]
      8439 38DF 228D 22F7 B374  2BC0 D94A A3F0 EFE2 1092
uid           [ unknown] Ubuntu CD Image Automatic Signing Key (2012) <cdimage@ubuntu.com>

/etc/apt/trusted.gpg.d/ubuntu-keyring-2018-archive.gpg
------------------------------------------------------
pub   rsa4096 2018-09-17 [SC]
      F6EC B376 2474 EDA9 D21B  7022 8719 20D1 991B C93C
uid           [ unknown] Ubuntu Archive Automatic Signing Key (2018) <ftpmaster@ubuntu.com>

/etc/apt/trusted.gpg.d/vivaldi-4218647E.gpg
-------------------------------------------
pub   rsa4096 2022-01-31 [SC] [expires: 2024-01-31]
      DF44 CF0E 1930 9195 C106  9AFE 6299 3C72 4218 647E
uid           [ unknown] Vivaldi Package Composer KEY08 <packager@vivaldi.com>
sub   rsa4096 2022-01-31 [E] [expires: 2024-01-31]

/etc/apt/trusted.gpg.d/vivaldi-C27AA466.gpg
-------------------------------------------
pub   rsa4096 2021-01-06 [SC] [expires: 2023-02-25]
      CB63 144F 1BA3 1BC3 9E27  79A8 FEB6 023D C27A A466
uid           [ unknown] Vivaldi Package Composer KEY07 <packager@vivaldi.com>
sub   rsa4096 2021-01-06 [E] [expires: 2023-02-25]
```

What follows is a log of the commands I issued (manually) on Jammy (x230t) to get rid
of these warnings.
```
apt-key export FB006459 | gpg --dearmour -o /etc/apt/trusted.gpg.d/digikam-savoury1.gpg
apt-key del FB006459

apt-key export 57F6FB06 | gpg --dearmour -o /etc/apt/trusted.gpg.d/signal-whispersystems.gpg
apt-key del 57F6FB06

apt-key export 97CE5D8D | gpg --dearmour -o /etc/apt/trusted.gpg.d/freerdp-nightly.gpg
apt-key del 97CE5D8D

apt-key export 41D01538 | gpg --dearmour -o /etc/apt/trusted.gpg.d/riot-matrix.gpg
apt-key del 41D01538

apt-key export 3F055C03 | gpg --dearmour -o /etc/apt/trusted.gpg.d/danielrichter-ppa-grub-customizer.gpg
apt-key del 3F055C03

apt-key export 8A8F901A | gpg --dearmour -o /etc/apt/trusted.gpg.d/sublimetext.gpg
apt-key del 8A8F901A

apt-key export 1378B444 | gpg --dearmour -o /etc/apt/trusted.gpg.d/libreoffice.gpg
apt-key del 1378B444

apt-key export CE49EC21 | gpg --dearmour -o /etc/apt/trusted.gpg.d/mozilla-ppa.gpg
apt-key del CE49EC21

apt-key export 76BAFBC6 | gpg --dearmour -o /etc/apt/trusted.gpg.d/peek-ppa.gpg
apt-key del 76BAFBC6

apt-key export 957F5868 | gpg --dearmour -o /etc/apt/trusted.gpg.d/tailscale.gpg
apt-key del 957F5868

apt-key export AD3DD469 | gpg --dearmour -o /etc/apt/trusted.gpg.d/nextcloud-ppa.gpg
apt-key del AD3DD469

apt-key export 28C3BA9D | gpg --dearmour -o /etc/apt/trusted.gpg.d/tmux-xpanes-greymd-ppa.gpg
apt-key del 28C3BA9D

apt-key export DE9E3B09 | gpg --dearmour -o /etc/apt/trusted.gpg.d/atom-editor.gpg
apt-key del DE9E3B09

apt-key export 61A7C71D | gpg --dearmour -o /etc/apt/trusted.gpg.d/zoom.gpg
apt-key del 61A7C71D

apt-key export 47878405 | gpg --dearmour -o /etc/apt/trusted.gpg.d/qownnotes-patrizio-bekerle-ppa.gpg
apt-key del 47878405

apt-key export A7803E3B | gpg --dearmour -o /etc/apt/trusted.gpg.d/foliate-archisman-panigrahi-ppa.gpg
apt-key del A7803E3B

apt-key export F684460C | gpg --dearmour -o /etc/apt/trusted.gpg.d/firejail-reinerherrman-ppa.gpg
apt-key del F684460C

apt-key export 2980AECF | gpg --dearmour -o /etc/apt/trusted.gpg.d/virtualbox.gpg
apt-key del 2980AECF

apt-key export 60D8DA0B | gpg --dearmour -o /etc/apt/trusted.gpg.d/boot-repair-yannubuntu.gpg
apt-key del 60D8DA0B

apt-key export 1118213C | gpg --dearmour -o /etc/apt/trusted.gpg.d/graphics-drivers-ppa.gpg
apt-key del 1118213C

apt-key export EE907473 | gpg --dearmour -o /etc/apt/trusted.gpg.d/solaar-logitech-ppa.gpg
apt-key del EE907473

apt-key export DC058F40 | gpg --dearmour -o /etc/apt/trusted.gpg.d/chromium-beta-saiarcot895-ppa.gpg
apt-key del DC058F40

apt-key del 98AB5139
apt-key del C27AA466
apt-key del 8DE1B7A0
apt-key del D38B4796
apt-key del 7FAC5991
apt-key del C2D4E821

cd /etc/apt/trusted.gpg.d/
rename 's/\.gpg~/.gpg/' *.gpg~
```

Note: for some reason, the gpg files exported in `/etc/apt/trusted.gpg.d/`
all have the file extension `.gpg~`, so they won't be picked up until that
symbol is removed. Weird.

We achieved a clean `apt update`:
```
$ sudo apt update
Hit:1 http://de.archive.ubuntu.com/ubuntu jammy InRelease
Hit:2 http://ppa.launchpad.net/savoury1/digikam/ubuntu jammy InRelease
Hit:3 http://pub.freerdp.com/repositories/deb/jammy freerdp-nightly InRelease
Hit:4 http://de.archive.ubuntu.com/ubuntu jammy-updates InRelease     
Ign:5 https://repo.vivaldi.com/stable/deb stable InRelease            
Hit:6 https://brave-browser-apt-release.s3.brave.com stable InRelease 
Hit:7 https://deb.opera.com/opera-stable stable InRelease             
Hit:8 https://updates.signal.org/desktop/apt xenial InRelease         
Hit:9 https://dl.google.com/linux/chrome/deb stable InRelease         
Hit:10 https://repo.vivaldi.com/stable/deb stable Release             
Hit:11 https://packages.riot.im/debian default InRelease              
Hit:12 http://de.archive.ubuntu.com/ubuntu jammy-backports InRelease  
Hit:13 http://ppa.launchpad.net/savoury1/ffmpeg4/ubuntu jammy InRelease
Hit:14 http://de.archive.ubuntu.com/ubuntu jammy-security InRelease   
Hit:15 http://ppa.launchpad.net/deki/firejail/ubuntu jammy InRelease  
Hit:16 http://ppa.launchpad.net/apandada1/foliate/ubuntu jammy InRelease
Hit:17 https://download.sublimetext.com apt/stable/ InRelease         
Hit:18 http://ppa.launchpad.net/danielrichter2007/grub-customizer/ubuntu jammy InRelease                          
Hit:19 http://ppa.launchpad.net/libreoffice/ppa/ubuntu jammy InRelease                      
Hit:20 http://ppa.launchpad.net/mozillateam/ppa/ubuntu jammy InRelease
Hit:21 http://ppa.launchpad.net/peek-developers/stable/ubuntu jammy InRelease
Get:22 https://pkgs.tailscale.com/stable/ubuntu jammy InRelease
Hit:24 http://ppa.launchpad.net/nextcloud-devs/client/ubuntu jammy InRelease                      
Hit:25 http://ppa.launchpad.net/pbek/qownnotes/ubuntu jammy InRelease                                               
Hit:26 http://ppa.launchpad.net/greymd/tmux-xpanes/ubuntu jammy InRelease                                           
Hit:23 https://packagecloud.io/AtomEditor/atom/any any InRelease
Fetched 5,542 B in 5s (1,198 B/s)
Reading package lists... Done
Building dependency tree... Done
Reading state information... Done
44 packages can be upgraded. Run 'apt list --upgradable' to see them.
```

+ https://linuxhint.com/change-file-extension-multiple-files-bash/
