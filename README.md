# common

Ansible role that configures aptitude sources, and installs a suite
of software packages common to all my servers.


## Links and notes

+ https://repolib.readthedocs.io/en/latest/deb822-format.html
+ https://wiki.debian.org/SourcesList
+ https://docs.ansible.com/ansible/latest/collections/ansible/builtin/deb822_repository_module.html
+ [Example usages of the new `deb822_repository` Ansible module](https://gist.github.com/roib20/27fde10af195cee1c1f8ac5f68be7e9b)
